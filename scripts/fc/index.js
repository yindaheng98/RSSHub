const proxy = require('@webserverless/fc-express');

const app = require('../../lib/app');
const server = new proxy.Server(app.callback());
module.exports.handler = function (req, res, context) {
    server.httpProxy(req, res, context);
};
